# Changelog

All notable changes to `Local baselocal/base` will be documented in this file

## 1.0.0 - 2017-12-24

- initial release
