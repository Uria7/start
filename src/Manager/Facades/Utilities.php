<?php

namespace local\Base\Manager\Facades;

use \Illuminate\Support\Facades\Facade;


/**
 * @see \local\Base\Manager\Controllers\Utilities
 */
class Utilities extends Facade {

    protected static function getFacadeAccessor() {
        return 'Utilities';
    }

}
