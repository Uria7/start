<?php

namespace local\Base\Manager\Traits;

use Agent;
use GeoIP;

trait UserAgent {

    public function addUserAgent($isRequest = NULL) {
        
            $request = (!empty($isRequest))? $isRequest: request();
       

            $this->languages = Agent::languages();

            $platform = Agent::platform();
            $this->platform = $platform;
            $this->platformVersion = Agent::version($platform);


            $browser = Agent::browser();
            $this->browser = $browser;
            $this->browserVersion = Agent::version($browser);


            $this->isDesktop = Agent::isDesktop();
            $this->isRobot = Agent::isRobot();
            $this->device = Agent::device();

            $this->HTTP_USER_AGENT = isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';

            
            

            $this->ip = $request->ip();

            $this->ips = $request->ips();

            $this->geoIP = GeoIP::getLocation()->toArray();
            
            
            
            return $this;
    }

}
