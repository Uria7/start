<?php

namespace local\Base\Manager\Traits;

use local\Base\Manager\Facades\Utilities;
//Helpers and Class
use Illuminate\Support\Facades\View;

trait DynamicInputs {

    protected $templateFormSeo = 'baseViews::layouts.pageFormSeo';
    protected $templateForm = 'baseViews::layouts.pageForm';
    protected $templateNotFound = 'baseViews::UI.notFound';

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $clave
     * @return \Illuminate\Http\Response
     */
    public function processPageUI($configPage, $dataPage) {

        $data = array_merge($dataPage, $configPage);

        

        $data['layout'] = ($data['isSeo']) ? $this->templateFormSeo : $this->templateForm;

        //$data['layout'] = $this->templateFormSeo;

        $data['htmlInputs'] = $this->porcessSectionsUI($data['sections'], $data['dataPage']);

        return $data;
    }

    protected function porcessSectionsUI($sections, $dataPage) {
$html = '';

        //dd($sections);

        foreach ($sections as $key => $item) {

            $item['name'] = $key;


            if ($item['element'] == 'wrapSection') {

                $subSections = $this->pushElemnetArrayMultiple($item['sections'], 'postfix', $key);


                $itemsPage = (isset($dataPage[$key])) ? $dataPage[$key] : $subSections;

                foreach ($itemsPage as $itemPage) {

                    $item['htmlElements'][] = $this->porcessSectionsUI($subSections, $itemPage);
                }
            } else {

//                if (!isset($item['value'])) {
//                    if (!empty(old($key))) {
//                        $item['value'] = old($key);
//                    } else {
//                        $item['value'] = (isset($dataPage[$key])) ? $dataPage[$key] : '';
//                    }
//                }

                $item['value'] = $this->defaultValue($dataPage, $item, $key);


                
               
                
            }

            $html .= $this->porcessElementUI($item);

            // agregando la nueva propiedad

            // if(isset($item['prop'])){
            //     //dd($item);
            //     $porciones = explode("required", $html);
            //     $html = $porciones[0]."required data-service-categorias='route(".$item['prop'].")'".$porciones[1];
            //     //dd($html);
            // }

        }


        return $html;
    }

    protected function porcessElementUI($item, $base = 'baseViews::UI') {

        //dd($item);

        $type = $item['element'];

//Obtiene nombre de template para UI
        $path = $base . '.' . $type;

        if (!View::exists($path)) {
            $path = $this->templateNotFound;
        }

        return View::make($path, ['item' => $item])->render();
    }

//    Agrupa elementos con el mismo name en un array

    public function generateArrayPostfix($postfix, $sections, $input) {


        $arrays = [];
        $arrayPostfix = [];
        foreach ($sections as $key => $value) {
            $arrayPostfix[] = $keyPostfix = $key . $postfix;
            $arrays[$key] = $input[$keyPostfix];
        }
        $first = current($arrayPostfix);



        return Utilities::combine_keys_with_arrays(array_keys($input[$first]), $arrays);
    }

//    Agrega un elemento a cada un

    public function pushElemnetArrayMultiple($multipleArray, $keyValue, $element) {

        foreach ($multipleArray as $key => $item) {
            $multipleArray[$key][$keyValue] = $element;
        }
        return$multipleArray;
    }

    public function defaultValue($dataPage, $item, $key) {
        $value = '';

        //dd($item);

        if (isset($item['value'])) {
            $value = $item['value'];
        } else if (!empty(old($key))) {
            $value = old($key);
        } else {
            $value = (isset($dataPage[$key])) ? $dataPage[$key] : '';
        }

        // Si tiene alguna otra propiedad
        // if(isset($item['property'])){
        //     $value = (isset($dataPage[]))
        // }

       // dd($value);
        return $value;
    }
    
        public function dynamicFillSave($pagina, $sections, $input) {


         foreach ($sections as $key => $item) {



            if ($item['element'] == 'wrapSection') {

                $pagina->{$key} = $this->generateArrayPostfix($key, $item['sections'], $input);
            } else {

                if (isset($input[$key])) {

                    $pagina->{$key} = $input[$key];
                }
            }
        }
        



        return $pagina;
    }


}
