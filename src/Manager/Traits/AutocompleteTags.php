<?php

namespace local\Base\Manager\Traits;

use local\Base\Manager\Facades\Utilities;
//Helpers and Class
use Illuminate\Http\Request;

trait AutocompleteTags {

    public $autocompleteResource;

    /**
     * Get json tags
     *
     * @param  Request  $request
     * @return json
     */
    public function getTags(Request $request) {

        $input = $request->all();

        $locale = isset($input['locale']) ? $input['locale'] : '*';

        $temp = Utilities::explodeTags($input['term']);
        $term = end($temp);
        $resource = $this->autocompleteResource;

        $tagsRresult = $resource::getTags($term, $locale);
        $data = array();


        foreach ($tagsRresult as $registro) {
            $temp = [];

            $temp['id'] = $registro['_id'];
            $temp['label'] = $registro['_id'];
            $temp['value'] = $registro['_id'];


            $data[] = $temp;
        }


        return response()->json($data);
    }

    /**
     * Get json tags
     *
     * @param  Request  $request
     * @return json
     */
    public function getCategories(Request $request) {

        $input = $request->all();

        $locale = isset($input['locale']) ? $input['locale'] : '*';

        $temp = Utilities::explodeTags($input['term'], $locale);
        $term = end($temp);

        $resource = $this->autocompleteResource;

        $tagsRresult = $resource::getCategories($term);
        $data = array();


        foreach ($tagsRresult as $registro) {
            $temp = [];

            $temp['id'] = $registro['_id'];
            $temp['label'] = $registro['_id'];
            $temp['value'] = $registro['_id'];


            $data[] = $temp;
        }


        return response()->json($data);
    }

}
