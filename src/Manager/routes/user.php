<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//User
if (!Route::has('manager.user.dataTable')){
    Route::post('user/dataTable/{id?}/{excel?}', array('as' => 'manager.user.dataTable', 'uses' => 'UserDataTableController@dataTable'));
}
if (!Route::has('manager.user.destroy')){
    Route::get('user/{id}/delete', ['as' => 'manager.user.destroy', 'uses' => 'UserController@destroy']);    
}

if (!Route::has('manager.user.index')){
    Route::get('index', ['as' => 'manager.user.index', 'uses' => 'UserController@index']);    
}

if (!Route::has('manager.user.create')){
    Route::get('create', ['as' => 'manager.user.create', 'uses' => 'UserController@create']);    
}
if (!Route::has('manager.user.store')){
    Route::post('store', ['as' => 'manager.user.store', 'uses' => 'UserController@store']);    
}
if (!Route::has('manager.user.show')){
    Route::get('show', ['as' => 'manager.user.show', 'uses' => 'UserController@show']);    
}
if (!Route::has('manager.user.edit')){
    Route::get('edit', ['as' => 'manager.user.edit', 'uses' => 'UserController@edit']);    
}
if (!Route::has('manager.user.update')){
    Route::put('update', ['as' => 'manager.user.update', 'uses' => 'UserController@update']);    
}


