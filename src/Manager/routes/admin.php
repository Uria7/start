<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
//Admin
Route::post('admin/dataTable/{id?}/{excel?}', array('as' => 'manager.admin.dataTable', 'uses' => 'AdminDataTableController@dataTable'));
Route::get('admin/{id}/delete', ['as' => 'manager.admin.destroy', 'uses' => 'AdminController@destroy']);
Route::resource('admin', 'AdminController', ['names' => [
        'index' => 'manager.admin.index',
        'create' => 'manager.admin.create',
        'store' => 'manager.admin.store',
        'show' => 'manager.admin.show',
        'edit' => 'manager.admin.edit',
        'update' => 'manager.admin.update',
//        'destroy' => 'manager.admin.destroy',
]]);


