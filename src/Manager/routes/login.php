<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//Login
    Route::get('/', ['as' => 'manager.index', 'uses' => 'LoginController@viewLogin']);

    Route::post('login', ['as' => 'manager.login', 'uses' => 'LoginController@postLogin']);

    Route::get('logout', ['as' => 'manager.logout', 'uses' => 'LoginController@getLogout']);
