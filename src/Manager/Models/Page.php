<?php

namespace local\Base\Manager\Models;

use local\Base\Manager\Models\Base;


class Page extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];



}


