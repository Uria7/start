<?php

namespace local\Base\Manager\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


use local\Base\Manager\Models\Base;

class Admin extends Base implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword;
    
    
    use Notifiable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'users';


    protected $collection = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        "nombre",
        "email",
        "password",
        "imagen",
        "genero",
        "descripcion",
        "estatus",
        "rol"
       
    ];
    protected $guarded = [
        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($value) {

        $this->attributes['password'] = bcrypt($value);
    }

    

   

}
