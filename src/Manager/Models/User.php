<?php

namespace local\Base\Manager\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use local\Base\Manager\Models\Base;
use local\Base\Manager\Traits\UserAgent;

class User extends Base implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {

    use Authenticatable,
        Authorizable,
        CanResetPassword;

    use Notifiable;
    
    use UserAgent;

    /**
     * The database table used by the model.
     *
     * @var string
     */
//    protected $table = 'users';


    protected $collection = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "nombre",
        "apellido",
        "email",
        "password",
        "imagen",
        "telefono",
        "ciudad",
        "edad",
        "rol",
        "genero",
        "descripcion",
        "status",
        "carrito",
        "idPayer",
        "recibo",
        "razonSocial",
        "rfc",
        "domicilioFiscal",
        "terminos",
        "newsletter",
        
        "tipo",
        "pais",
        "estado",
        
    ];
    protected $guarded = [
        "rol",
        "clave"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    protected $casts = [
        'estatus' => 'boolean',
//            'carrito' => 'array',
    ];

    public function setPasswordAttribute($value) {

        $this->attributes['password'] = bcrypt($value);
    }
    
     public function getSecretAttribute() {
        return base64_decode($this->attributes['secret']);
    }

    public function paypal() {
        return $this->hasMany(\local\Paypal\Manager\Models\Paypal::class);
    }

    public function paypalPlan() {
        return $this->hasMany(\local\Paypal\Manager\Models\PaypalPlan::class);
    }

    public function paypalLogs() {
        return $this->hasMany(\local\Paypal\Manager\Models\PaypalLog::class);
    }

//    public function contadorCarrito() {
//
//
//        if (isset($this->carrito)) {
//
//            $carrito = $this->carrito;
//
//            $contadorCarrito = array_sum($carrito);
//        } else {
//            $contadorCarrito = 0;
//        }
//
//        return $contadorCarrito;
//    }

}
