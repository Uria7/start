<?php

namespace local\Base\Manager\Models;

use MongoDB\BSON\UTCDatetime;
use Jenssegers\Mongodb\Eloquent\Model;
use local\Base\Manager\Facades\Utilities;

class Base extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = '';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'longTitle', 'description', 'content', 'thumbnail', 'image', 'gallery', 'video', 'slug', 'tags', 'category', 'url', 'type', 'status', 'datePublication', 'order', 'icon', 'locale', 'publicacion',
        'titleSeo', 'descriptionSeo', 'keywordsSeo', 'imageSeo'
    ];
    protected $guarded = [
        "clave"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    protected $casts = [
        'status' => 'boolean',
        'order' => 'integer',
        'publicacion' => 'boolean'
    ];
    protected $dates = ['datePublication'];

    public function setDatePublicationAttribute($value) {

        $this->attributes['datePublication'] = new UTCDatetime(strtotime($value) * 1000);
    }

    public function setFillable($fields) {
        
        $this->fillable =  array_unique (array_merge($this->fillable, $fields));
    }

    public function getTagsAttribute($array) {
        
        if (is_array($array)){
            return Utilities::implodeTags($array, ',');
        }
        return $array;
        
    }

    public function getCategoryAttribute($array) {
        if (is_array($array)){
            return Utilities::implodeTags($array, ',');
        }
        return $array;
    }

    public static function getTags($term = '', $locale = '*') {
        $query = [
            [
                '$unwind' => '$tags'
            ],
            [
                '$match' => [
                    'locale' => $locale,
                    'tags' => [
                        '$regex' => $term,
                        '$options' => 'i'
                    ],
                ]
            ],
            [
                '$group' => [
                    '_id' => '$tags',
                    'count' => [
                        '$sum' => 1
                    ]
                ]
            ]
        ];


        if ($locale == '*') {
            unset($query[1]['$match']['locale']);
        }




        $tags = self::raw(function($collection) use ($query) {
                    return $collection->aggregate($query);
                });


        foreach ($tags as $item) {

            if (isset($item->_id) && gettype($item->_id) == "object" && get_class($item->_id) == "MongoDB\Model\BSONArray") {

                $bsonSerialize = $item->_id->bsonSerialize();

                $item->_id = array_shift($bsonSerialize);
            }
        }



        return $tags;
    }

    public static function getCategories($term = '', $locale = '*') {




        $query = [
            [
                '$match' => [
                    'locale' => $locale,
                    'category' => [
                        '$regex' => $term,
                        '$options' => 'i',
                    ],
                ]
            ],
            [
                '$group' => [
                    '_id' => '$category',
                    'count' => [
                        '$sum' => 1
                    ],
                ]
            ]
        ];



        if ($locale == '*') {
            unset($query[0]['$match']['locale']);
        }



        $categorias = self::raw(function($collection) use ($query) {
                    return $collection->aggregate($query);
                });



        foreach ($categorias as $item) {

            if (isset($item->_id) && gettype($item->_id) == "object" && get_class($item->_id) == "MongoDB\Model\BSONArray") {

                $bsonSerialize = $item->_id->bsonSerialize();

                $item->_id = array_shift($bsonSerialize);
            }
        }



        return $categorias;
    }

    public function setStatusAttribute($value) {

        if ($value === TRUE || $value === 'true' || $value === 'TRUE' || $value === 1 || $value === '1') {
            $this->attributes['status'] = TRUE;
        } else {
            $this->attributes['status'] = FALSE;
        }
    }

    public function setTagsAttribute($value) {


        $this->attributes['tags'] = Utilities::explodeTags($value);
    }

    public function setCategoryAttribute($value) {


        $this->attributes['category'] = Utilities::explodeTags($value);
    }

    public function setOrderAttribute($value) {



        $this->attributes['order'] = intval($value);
    }

}
