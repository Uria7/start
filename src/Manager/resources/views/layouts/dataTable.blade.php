@extends('baseViews::layouts.base')


@push('css-lib')
<!-- DataTables CSS -->
<link type="text/css" href="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link type="text/css" href="admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">


@endpush


@section('content')


<div id="wrapper">

    @include('baseViews::includes.nav') 

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa @yield('sectionIcon','fa-graduation-cap')"></i> @yield('sectionName')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('sectionAction') @yield('sectionName')
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">

						

                          @if(isset($s))
                        <input type="hidden" id="searchInput" value="{{$s}}" >
                        @endif



                        <div class="dataTable_wrapper">

							<form method="POST" action="@yield('formAction')/0/1"  class="form-inline" >
								<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">


								
								
								
								
								<h5>Filtros:</h5>


								@yield('inputs')






								<hr>
								<div style="clear: both;" ></div>


								<div class="table-responsive">

									<table class="table table-striped table-bordered table-hover datatable" id="dataTables-example" data-url="@yield('formAction')">

										<thead class="no-border">

											<tr>

												@yield('dataTableColums')
											</tr>

										</thead>

									</table>
								</div>
							</form>


                        </div>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@endsection

@push('js-lib')

<!-- DataTables JavaScript -->
<script src="admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

@endpush





