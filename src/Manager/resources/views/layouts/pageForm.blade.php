@extends('baseViews::layouts.base')


@push('css-lib')

<link type="text/css" href="admin/bower_components/cropperjs/dist/cropper.min.css" rel="stylesheet">
@endpush


@section('content')


<div id="wrapper">

    @include('baseViews::includes.nav') 

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-fw @yield('sectionIcon','fa-pencil')"></i> @yield('sectionName')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('sectionAction') @yield('sectionName')
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <div class="col-lg-12">


                                <form role="form" action="@yield('formAction')" method="POST" enctype="multipart/form-data" class="validate" >
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                                    @yield('inputs')

                                    @section('buttons')
                                    <button type="submit" class="btn btn-default">Guardar</button>
                                    @show
                                </form>


                            </div>

                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->





@endsection

@push('js-lib')


<script src="admin/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>


<script src="admin/bower_components/cropperjs/dist/cropper.min.js"></script>

@endpush




