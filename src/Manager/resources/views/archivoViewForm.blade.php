@extends('baseViews::layouts.pageForm')


@section('sectionAction', 'Vista')
@section('sectionName', 'Archivo')
@section('formAction', '')

@section('inputs')



<div class="form-group">
    <label>Ruta Archivo</label>

    <a target="_blank" class="btn btn-default btn-block" href="{{ $path }}" ><i class="fa fa-download">Descargar</i></a>


    <input type="text" class="form-control" name="path" value="{{ $path }}"  >


</div>





<a href="{{ route('manager.fileUpload.create') }}" class="btn btn-default">Nuevo archivo</a>





@endsection




