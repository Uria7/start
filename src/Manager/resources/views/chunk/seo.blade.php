HERE: 

<div class="form-group">
    <label>Título</label>
    <input type="text" class="form-control" name="titleSeo" value="{{ $dataPage['titleSeo'] or '' }}" required maxlength="60" >
    <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"></span>   </p>
</div>

<div class="form-group">
    <label>Descripción Corta</label>
    <input type="text" class="form-control" name="descriptionSeo" value="{{ $dataPage['descriptionSeo'] or '' }}" required maxlength="155" >
    <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"></span>   </p>
</div>

<div class="form-group">
    <label>Palabras Clave</label>
    <textarea class="form-control mceNoEditor" rows="3" name="keywordsSeo" required >{{ $dataPage['keywordsSeo'] or '' }}</textarea>
    <p class="help-block contador">Palabras clave separadas 'palabra, palabra'</p>
</div>



<div class="form-group">


    <!--                                        <button class="btn btn-success btn-sm addInput  form-control-static" type="button">
        <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span> Agregar otra imagen
    </button>-->


    <div class="row campoInput">




        <div class="col-md-8 text-center fileImageUploadPreview ">

            <img  class="img-responsive  form-control-static"    src="{{ $dataPage['imageSeo'] or '' }}">


            <input type="text" name="imageSeo"  class="hiddenImagePath form-control" value="{{ $dataPage['imageSeo'] or '' }}"  required="" >

            <p class="help-block">Minimo 500px ancho</p>
        </div>


        <div class="col-md-4 text-center ">

            <button type="button" class="btn btn-primary btn-md lunchImageModal "

                    data-url="{{ route('manager.image.load') }}"
                    data-target="#imageModalCrop"
                    data-toggle="modal"
                    data-resize="resize"
                    data-min-width="0"
                    data-min-height="0"
                    data-aspect-ratio="NaN"
                    data-path="img/pages"
                    data-format="jpg"  data-quality="80"
                    data-prefix=""

                    >
                Cargar imagen
            </button>
        </div>
    </div>



</div>

