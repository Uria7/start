@extends($layout)


@section('sectionAction', 'Editar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.admin.update',[$record->id]))

@section('inputs')
{{ method_field('PUT') }}

{!! $htmlInputs !!}

@endsection




