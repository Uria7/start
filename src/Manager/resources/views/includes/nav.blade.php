<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="{{config('manager.base.manager.config.logo')}}" class="img-responsive" alt="Logo" style="width: 25%"></a>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-right">
        <!-- /.dropdown -->
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                {{$user->email}}
                @if(isset($user->imagen))
                <img src="{{  Utilities::getDataUrlThumbnail($user->imagen, 45, 45) }}" class="img-responsive img-thumbnail" >
                @else 
                <i class="fa fa-user fa-fw"></i>
                @endif 
                <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                @if($user->rol=="admin")
                <li><a href="{{ route('manager.admin.edit', [$user->_id]) }}"><i class="fa fa-user fa-fw"></i> Perfil</a>
                </li>
                <li class="divider"></li>
                @endif
                <li><a href="{{ route('manager.logout') }}" ><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">

                @if(config('manager.base.manager.crm'))

                <li>
                    <a href="#">
                        <i class="fa fa-bar-chart-o fa-fw"></i> CRM<span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level">


                        @each('baseViews::chunk.menuModules', config('manager.base.manager.crm'), 'item')

                    </ul>
                    <!-- /.nav-second-level -->
                </li>


                @endif


                @each('baseViews::chunk.menuCMS', \LaravelLocalization::getSupportedLocales(), 'item')


                @each('baseViews::chunk.menuModules', config('manager.base.manager.modules'), 'item')
                
                
                
                
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>

