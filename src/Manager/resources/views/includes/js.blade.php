<!-- Default JS -->
@section('js-default')
<!-- jQuery -->

<script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>

  <script type="text/javascript" src="admin/js/tinymce_es.js"></script>

    <script type="text/javascript" src="admin/bower_components/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript" src="admin/js/jquery-ui.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script type="text/javascript" src="admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <script type="text/javascript"  src="admin/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript"  src="admin/bower_components/jquery-validation/src/localization/messages_es.js"></script>


    <!-- Custom Theme JavaScript -->
    <script type="text/javascript"  src="admin/dist/js/sb-admin-2.js"></script>



@show

<!-- LIB JS Opcionales que se remplazan en vistas-->
@stack('js-lib')


<!-- script JS -->
@section('js-script')
<script  type="text/javascript" src="admin/js/scripts.js?id={{ uniqid() }}"></script>
<script  type="text/javascript" src="admin/js/upload-file.js"></script>
@show



