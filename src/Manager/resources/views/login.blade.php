@extends('baseViews::layouts.base')


@section('content')




<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <br>
            <img src="{{config('manager.base.manager.config.logo')}}" class="center-block img-responsive" alt="Logo">
                
            <div class="login-panel panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title">Inicio de sesi&oacute;n</h3>
                </div>
                <div class="panel-body">
                         <form role="form" action="{{ route('manager.login') }}" method="POST" >
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="recuerdame" type="checkbox" value="recordar">Recordar
                                </label>
                            </div>
                            <button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection






