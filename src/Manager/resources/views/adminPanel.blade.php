@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Administradores')
@section('formAction', route('manager.admin.dataTable'))


@section('inputs')


<div class="form-group">
	<label for="rol">Rol</label> <br>

	<select class="reloadTable" name="rol">
		<option value="">Ninguno</option>
		<option value="admin">Administrador</option>
		<option value="editor">Editor</option>


	</select>
</div>




<div class="form-group">
	<label for="status">Estatus</label> <br>

	<select class="reloadTable" name="status">
		<option value="">Ninguno</option>
		<option value="true">Activo</option>
		<option value="false">Desactivado</option>


	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

	<div class="btn-group" role="group" aria-label="...">
		<a class="btn btn-success " href="{{ route('manager.admin.create')}}" >

			<i class="fa fa-plus-circle"></i> NUEVO

		</a>
	</div>
	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>

@endsection

@section('dataTableColums')



<th data-details="true" ></th>


<th data-name="nombre" data-orderable="true"  data-order="asc">
	<strong>Nombre</strong>
</th>


<th data-name="email" data-orderable="true"  >
	<strong>Email</strong>
</th>


<th data-name="rol" data-orderable="true">
	<strong>Rol</strong>
</th>


<th data-name="genero" data-orderable="true"  >
	<strong>Genero</strong>
</th>



<th data-name="status" data-orderable="true">
	<strong>Estatus</strong>
</th>
<th data-name="created_at"><strong>Fecha Alta</strong></th>

<th data-name="opciones">
	<strong>Opciones</strong>
</th>



@endsection