﻿<?php
$label = isset($item['label']) ? $item['label'] : 'No título';
$name = isset($item['name']) ? $item['name'] : '';
$value = isset($item['value']) ? $item['value'] : '';
$required = isset($item['required']) && $item['required'] ? 'required' : '';
$postfix = isset($item['postfix']) ? $item['postfix'].'[]' : '';
$prefix = isset($item['prefix']) ? $item['prefix'] : '';
$type = isset($item['type']) ? $item['type'] : 'text';
$maxlength = isset($item['maxlength']) ? 'maxlength="'.$item['maxlength'].'"' : '';
$helper = isset($item['helper']) ? $item['helper'] : '';
$maxValue = isset($item['maxValue']) ? $item['maxValue'] : ''; 
 
$dataServiceCategories= isset($item['dataServiceCategories']) ? route($item['dataServiceCategories']) : FALSE;

$id = isset($item['id']) ? $item['id'] : FALSE;

$dataServiceTags = isset($item['dataServiceTags']) ? route($item['dataServiceTags']) : FALSE;


?>



<div class="form-group">
    <label>{{$label}}</label>

     <input type="{{$type}}"  @if($id)id="{{$id}}"@endif class="form-control" name="{{$prefix}}{{$name}}{{$postfix}}" value="{{$value}}" {{$required}} {{$maxlength}} @if($dataServiceCategories) data-service-categories="{{$dataServiceCategories}}" @endif
     @if($dataServiceTags) data-service-tags="{{$dataServiceTags}}" @endif 
	 @if($maxValue) max="{{$maxValue}}" @endif>


    @if($maxlength)
    <p class="help-block contador">Caracteres permitidos: <span class="total"></span>/<span class="hasta"> </span></p>
    @endif
    @if($helper)
    <p class="help-block">{{$helper}}</p>
    @endif
    
</div>
