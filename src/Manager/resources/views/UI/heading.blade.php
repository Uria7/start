<?php
$label = isset($item['label']) ? $item['label'] : 'No título';
$tag = isset($item['tag']) ? $item['tag'] : 'h3';

?>


<div class="form-group">
    <{{$tag}} class="form-control-static">{{$label}}</{{$tag}}>
</div>