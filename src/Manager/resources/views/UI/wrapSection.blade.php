<?php
$label = isset($item['label']) ? $item['label'] : 'No título';
$htmlElements = isset($item['htmlElements']) ? $item['htmlElements'] : [];
?>


<div class="form-group">

    <label>{{$label}}</label>
    
    <button class="btn btn-success btn-sm addInput  form-control-static" type="button">
            <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span> Agregar una nueva seccion
        </button>

    
    
    
    
    @foreach($htmlElements as $key => $item )
    <div class="row campoInput">
        
        {!! $item !!}

        @if($key >0)
        <div class="col-md-1">
            <button type="button" class="btn btn-warning btn-sm removeCampoInput" value="">
                <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>
            </button>
        </div>
        @endif


    </div>

    @endforeach

</div>


