<?php
$label = isset($item['label']) ? $item['label'] : 'No título';
$name = isset($item['name']) ? $item['name'] : '';
$value = isset($item['value']) ? $item['value'] : '';
$required = isset($item['required']) && $item['required'] ? 'required' : '';
$mceNoEditor = isset($item['mceNoEditor']) && $item['mceNoEditor'] ? 'mceNoEditor' : '';
$postfix = isset($item['postfix']) ? $item['postfix'].'[]' : '';
$prefix = isset($item['prefix']) ? $item['prefix'] : '';

?>

<div class="form-group">
    <label >{{$label}}</label>
    <textarea class="form-control {{$mceNoEditor}} " rows="3" name="{{$prefix}}{{$name}}{{$postfix}}" {{$required}} >{{$value}}</textarea>
</div>

