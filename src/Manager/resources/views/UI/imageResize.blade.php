<?php

$label = isset($item['label']) ? $item['label'] : '';
$name = isset($item['name']) ? $item['name'] : 'imagen';
$value = isset($item['value']) ? $item['value'] : '';
$required = isset($item['required']) && $item['required'] ? 'required' : '';
$postfix = isset($item['postfix']) ? $item['postfix'].'[]' : '';
$prefix = isset($item['prefix']) ? $item['prefix'] : '';



?>
<div class="form-group">

    
    <label>{{$label}}</label>

    <!--                                        <button class="btn btn-success btn-sm addInput  form-control-static" type="button">
        <span aria-hidden="true" class="glyphicon glyphicon-plus-sign"></span> Agregar otra imagen
    </button>-->


    <div class="row campoInput">




        <div class="col-md-8 text-center fileImageUploadPreview ">

            <img  class="img-responsive  form-control-static"    src="{{ $value }}">


            <input type="text" name="{{$prefix}}{{ $name }}{{ $postfix }}"  class="hiddenImagePath form-control" value="{{ $value }}"  {{$required}} >

                   <p class="help-block">http://placehold.it/{{$item['w'] or '1280' }}x{{$item['h']  or '720'}}</p>
        </div>


        <div class="col-md-4 text-center ">

            <button type="button" class="btn btn-primary btn-md lunchImageModal "

                    data-url="{{ route('manager.image.load') }}"
                    data-target="#imageModalCrop"
                    data-toggle="modal"
                    data-resize="{{$item['resize'] or 'widen'}}"
                    data-min-width="{{$item['w'] or '1280' }}"
                    data-min-height="{{$item['h'] or '720' }}"
                    data-aspect-ratio="{{ $item['ratio'] or '' }}"
                    data-path="{{ $item['path'] or 'img/page' }}"
                    data-format="{{ $item['format'] or 'jpg' }}"  
                    data-quality="{{ $item['quality'] or '100' }}"
                    data-prefix="{{ $item['prefix'] or '' }}"

                    >
                Cargar imagen
            </button>
        </div>
    </div>



</div>

