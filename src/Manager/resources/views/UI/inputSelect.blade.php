<?php
$label = isset($item['label']) ? $item['label'] : '';
$name = isset($item['name']) ? $item['name'] : '';
$value = isset($item['value']) ? $item['value'] : '';

if (is_bool($value)) {
    $value = ($item['value']) ? 'true' : 'false';
}

$required = isset($item['required']) && $item['required'] ? 'required' : '';
$postfix = isset($item['postfix']) ? $item['postfix'] . '[]' : '';
$prefix = isset($item['prefix']) ? $item['prefix'] : '';
$options = isset($item['options']) ? $item['options'] : [];

$helper = isset($item['helper']) ? $item['helper'] : '';

//
//dd($options,$value );
?>

<div class="form-group">
    <label>{{$label}} </label>
    <select name="{{$prefix}}{{$name}}{{$postfix}}" class="form-control" {{$required}} >

        @foreach($options as $key => $option)

        @if(is_array($option))
        <option value="{{ $option['value'] }}" @if($option['value'] === $value ) selected @endif>{{ $option['label'] }}</option>
        @else
        <option value="{{ $key }}" @if($key === $value ) selected @endif>{{ $option }}</option>
        @endif

        
        @endforeach

    </select>

    @if($helper)
    <p class="help-block">{{$helper}}</p>
    @endif
</div>

