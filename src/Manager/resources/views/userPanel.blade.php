@extends('baseViews::layouts.dataTable')


@section('sectionAction', 'Tabla')
@section('sectionName', 'Usuarios')
@section('formAction', route('manager.user.dataTable'))


@section('inputs')





<div class="form-group">
	<label for="recibo">Recibo</label> <br>

	<select class="reloadTable" name="recibo">
		<option value="">Ninguno</option>
		<option value="true">Si</option>
		<option value="false">No</option>


	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">

	
	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>

@endsection

@section('dataTableColums')



<th data-details="true" ></th>


<th data-name="idPayer" data-orderable="true"  data-order="asc">
	<strong>Id Paypal</strong>
</th>
<th data-name="nombre" data-orderable="true"  data-order="asc">
	<strong>Nombre</strong>
</th>

<th data-name="apellido" data-orderable="true"  data-order="asc">
	<strong>Nombre</strong>
</th>


<th data-name="email" data-orderable="true"  >
	<strong>Email</strong>
</th>

<th data-name="pais" data-orderable="true"  >
	<strong>País</strong>
</th>


<th data-name="recibo" data-orderable="true">
	<strong>Recibo</strong>
</th>
<th data-name="created_at"><strong>Fecha Alta</strong></th>

<th data-name="opciones">
	<strong>Opciones</strong>
</th>



@endsection