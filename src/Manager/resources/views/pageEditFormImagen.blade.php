@extends('baseViews::layouts.pageForm')


@section('sectionAction', 'Cargar')
@section('sectionName', 'Imagen')
@section('formAction', '')

@section('inputs')

<div class="row campoInput">




	<div class="col-md-8 text-center fileImageUploadPreview ">

		<img  class="img-responsive  form-control-static"    src="http://placehold.it/300x300" >


		<input type="text" name="imagen"  class="hiddenImagePath form-control" value="" >


	</div>


	<div class="col-md-4 text-center ">

		<button type="button" class="btn btn-primary btn-md lunchImageModal "

				data-url="{{ route('manager.image.load') }}"
				data-target="#imageModalCrop"
				data-toggle="modal"
				data-min-width="0"
				data-min-height="0"
				data-aspect-ratio="NaN"
				data-path="img/pages"
				data-format="png"  data-quality="90"
				data-base-url="full"
				data-toggle="modal"

				>
			Cargar imagen
		</button>
	</div>


</div>




@endsection



@section('buttons')




@endsection