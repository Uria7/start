@extends($layout)


@section('sectionAction', $sectionAction)
@section('sectionName', $sectionName)
@section('formAction', route('manager.pages.update',[$pagina->id]))

@section('inputs')


{{ method_field('PUT') }}

<input type="hidden" name="clave" value="{{$pagina->clave}}">
<input type="hidden" name="locale" value="{{$pagina->locale or 'es'}}">




{!! $htmlInputs !!}



@endsection
