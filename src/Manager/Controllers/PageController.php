<?php

namespace local\Base\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
use local\Base\Manager\Models\Page;
//Helpers and Class
use Illuminate\Http\Request;
use local\Base\Manager\Controllers\ManagerController;
use local\Base\Manager\Traits\DynamicInputs;

class PageController extends ManagerController {

    use DynamicInputs;
    protected $configPages = '';

    public function __construct() {
        parent::__construct();
        $this->configPages = config('manager.pages');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $clave
     * @return \Illuminate\Http\Response
     */
    public function edit($clave) {

        
        $locale = request('locale', 'es');

        $pagina = Page::where('clave', '=', $clave)->where('locale', $locale)->first();

        


        if (!isset($pagina->id)) {

            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        } else if (!isset($this->configPages[$clave])) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }



        $this->data['dataPage'] = $pagina->toArray();

        $this->data['pagina'] = $pagina;
        $configPage = $this->configPages[$clave];

        //dd("-->PageController",$configPage);
        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::pageEditFormCMS', $processData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $input = $request->all();


        $rules = array(
            'clave' => array('required', "exists:pages,clave"),
        );

        

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
//            return back()->withErrors($validator)->with([
//                        'error' => trans('baseLang::mensajes.registro.incompleto'),
//            ]);
        }




        $locale = request('locale', 'es');
        $clave = $request->input('clave');

        $pagina = Page::where('clave', '=', $clave)->where('locale', $locale)->first();

        



        if (!isset($pagina->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }

        $sections = $this->configPages[$clave]['sections'];




        $pagina->fill($input);


        $pagina = $this->dynamicFillSave($pagina, $sections, $input);


        $pagina->save();

        return redirect()->route('manager.pages.edit', ['clave' => $input['clave'], 'locale' => $input['locale']])->with([
                    'mensaje' => trans('baseLang::mensajes.registro.exito'),
        ]);
    }

}
