<?php

namespace local\Base\Manager\Controllers;

//Facades
use Validator;
use Auth;
use Route;
//Models
//Class
use local\Base\Manager\Controllers\ManagerController;
use Illuminate\Http\Request;

class LoginController extends ManagerController {

    protected $redirectAuth;

    public function __construct() {
        parent::__construct();

        $this->redirectAuth = config('manager.base.manager.redirectAuth');
    }

    public function viewLogin() {



        return view('baseViews::login', $this->data);
    }

    public function postLogin(Request $request) {

        $rules = array(
            'email' => array('required', 'email'),
            'password' => array('required', 'min:6'),
        );

        $input = $request->all();

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {


            return redirect()->route("manager.index")->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.usuario.noEncontrado')
                    ])->withInput($request->except('password'));
        } else {

            $admin = array(
                'email' => $input['email'],
                'password' => $input['password'],
                'status' => TRUE
            );

            $checkRecord = FALSE;
            if (isset($input['recuerdame'])) {
                $checkRecord = TRUE;
            }

            if (!Auth::attempt($admin, $checkRecord)) {

                return redirect()->route("manager.index")->withErrors($validator)->with([
                            'error' => trans('baseLang::mensajes.usuario.noEncontrado')
                        ])->withInput($request->except('password'));
            } else if (!Route::has($this->redirectAuth)) {
                return redirect()->route("manager.index")->withErrors($validator)->with([
                            'error' => trans('baseLang::mensajes.route.notFound')
                        ])->withInput($request->except('password'));
            } else {

                return redirect()->route($this->redirectAuth);
            }
        }
    }

    public function getLogout() {

        Auth::logout();
        return redirect()->route($this->redirectAuth);
    }


}
