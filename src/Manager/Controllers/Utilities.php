<?php

namespace local\Base\Manager\Controllers;


use MongoDB\BSON\Regex;
use MongoDB\BSON\ObjectID;

use local\Paypal\Manager\Models\Paypal;

class Utilities {


    public function sortBySubkey(&$array, $subkey, $sortType = SORT_ASC) {
        foreach ($array as $subarray) {
            $keys[] = $subarray[$subkey];
        }

        array_multisort($keys, $sortType, $array);
    }

    public function slug($string, $slug = '-', $maxlength = 1024) {
        $stringStrip = strip_tags($string);
        $seo = strtolower(trim(preg_replace('~[^0-9a-z]+~i', $slug, html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($stringStrip, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), $slug));

        if (strlen($seo) > $maxlength) {
            $seo = substr($seo, 0, $maxlength);
            $pos = (int) strrpos($seo, $slug); //ultima posicion de la busqueda
            if ($pos) {
                $seo = substr($seo, 0, $pos);
            }
        }

        return $seo;
    }

    public function explodeTags($stringTags, $delimiter = ",", $character_mask = " \t\n\r\0\x0B,.") {

        $temp = explode($delimiter, trim($stringTags, $character_mask));

        foreach ($temp as $key => $value) {

            $temp[$key] = trim($value, $character_mask);
        }
        return array_unique($temp);
    }

    public function implodeTags($arrayTags, $delimiter = " ", $slugMode = false) {

        if ($slugMode) {

            foreach ($arrayTags as $key => $value) {

                $arrayTags[$key] = $this->slug($value);
            }
        }



        return implode($delimiter, $arrayTags);
    }

    public function generateQuery($inputs = [], $searchable = [], $root = true) {

        $query = [];

        foreach ($inputs as $input => $value) {

            if (array_key_exists($input, $searchable) && $value) {

                if (is_array($searchable[$input])) {

                    $inputsTemp = [];
                    foreach ($searchable[$input] as $k => $v) {
                        $inputsTemp[$k] = $value;
                    }


                    $recursiveSerponse = $this->generateQuery($inputsTemp, $searchable[$input], false);
                    if (!empty($recursiveSerponse)) {

                        $query['$or'][] = $recursiveSerponse;
                    }
                } else if ($searchable[$input] == 'contains') {

                    $isMongoValid = $this->idMongoValid($value);

                    $response = null;

                    if ($input == '_id' && $isMongoValid) {
                        $response = new ObjectID($value);
                    } else if ($input != '_id') {
                        $response = ['$regex' => new Regex($value, "i")];
                    }

                    if ($root) {
                        $query[$input] = $response;
                    } else {
                        $query['$or'][][$input] = $response;
                    }
                } else if ($searchable[$input] == 'equal') {

                    if (in_array($value, ['true', 'false'])) {
                        $query[$input] = ($value == 'true' ) ? true : false;
                    } else if (is_array($value)) {
                        $query[$input]['$in'] = $value;
                    } else {
                        $query[$input] = (is_numeric($value)) ? (float) $value : $value;
                    }
                }
            }
        }

        return $query;
    }

    public function formatBytes($bytes, $force_unit = NULL, $format = NULL, $si = TRUE) {
        // Format string
        $format = ($format === NULL) ? '%01.2f %s' : (string) $format;

        // IEC prefixes (binary)
        if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE) {
            $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            $mod = 1024;
        }
        // SI prefixes (decimal)
        else {
            $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
            $mod = 1000;
        }

        // Determine unit to use
        if (($power = array_search((string) $force_unit, $units)) === FALSE) {
            $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
        }

        return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
    }

    public function combine_keys_with_arrays($keys, $arrays) {
        $results = array();

        foreach ($arrays as $subKey => $arr) {
            foreach ($keys as $index => $key) {
                $results[$key][$subKey] = $arr[$index];
            }
        }

        return $results;
    }

    public function getDataUrlThumbnail($url, $w = 100, $h = null, $resize = 'fit', $encode = 'jpg', $quality = 70) {

        if (\File::exists($url)) {

            //dd("Utilities, Existe el file");

            $img = \ImageInter::make($url);


            switch ($resize) {
                case 'fit':
                    $img = $img->fit((int) $w, (int) $h);

                    break;
                case 'widen':
                    $img = $img->widen((int) $w);

                    break;
                case 'heighten':
                    $img = $img->heighten((int) $h);
                    break;
                case 'resize':
                    $img = $img->resize((int) $w, (int) $h);
                    break;

                default:
                    $img = $img->fit((int) $w, (int) $h);
                    break;
            }



            return (string) $img->encode($encode, $quality)->encode('data-url');
        } else { 
            //dd("Utilities, No Existe el file");
            return "";
        }
    }

    public function getIdVimeoUrl($vimeo) {

        if (!empty($vimeo)) {

            return substr(parse_url($vimeo, PHP_URL_PATH), 1);
        } else {
            return "";
        }
    }

    public function idMongoValid($value) {
        if ($value instanceof \MongoDB\BSON\ObjectID) {
            return true;
        }
        try {
            new \MongoDB\BSON\ObjectID($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function isoDateToString($date){
       // Y-m-d H:i:s
        return $date->format('DD-MM-YYYY H:i:s');
    } 

    // Funcion para generar los PDF

    public function generateComprobantePdf(Paypal $pago, $format = 'raw', $prefix = 'comprobante_', $tipoComprobante = 'comprobante') {
            // Si tipoComprobante = comprobante, es un pago
            // Si tipoComprobante = donativo. es un donativo
            $user = $pago->user;
            $pdf;
            if($tipoComprobante == 'comprobante'){
                $pdf = \PDF::loadView('Pages.pdf.comprobante', ['pay' => $pago, 'user' => $user]);
            }else if($tipoComprobante == 'donativo'){
                $pdf = \PDF::loadView('Pages.pdf.comprobanteDonativo', ['pay' => $pago, 'user' => $user]);    
            }
            $storage_path = 'app/public/PDF/' . $prefix . $pago->id . '.pdf';
            $pdf->storagePath = $storage_path;
            $response = '';
            switch ($format) {
                case 'save':
                    $response = $pdf->save(storage_path($storage_path));
                    break;
                case 'download':
                    $response = $pdf->save(storage_path($storage_path))->download($prefix . $pago->id . '.pdf');
                    break;
                case 'stream':
                    $response = $pdf->stream($prefix . $pago->id . '.pdf');
                    break;
                case 'raw':
                    $response = false;
                    break;
            }
            //dd("El valor de respinse es: ",$response, "El valor de validacion es: ",$format);
            return $response;
    }
    
    
    
    public function objectToArray($object , $asociative = true) {
       return json_decode(json_encode($object), $asociative);

    }

}
