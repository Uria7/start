<?php

namespace local\Base\Manager\Controllers;

//Providers

use Auth;
    
use App\User;

use local\Base\Manager\Facades\Utilities;

//Models
//Helpers and Class

use local\Base\Manager\Controllers\ManagerController;

class UserController extends ManagerController {

    protected $returnRoute = 'manager.user.index';

    public function index() {

        

        $data = array();

        $data['user'] = Auth::user();

        


        return view('baseViews::userPanel', $data);
    }

   
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        

        $isValid = Utilities::idMongoValid($id);
        $user = User::findOrFail($id);

        if (!$isValid || !isset($user->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($user->status) {

            $user->status = FALSE;
        } else {
            $user->status = TRUE;
        }



        $user->save();


        return redirect()->route($this->returnRoute)->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
    }


}
