<?php

namespace local\Base\Manager\Controllers;

//Facades
use DB;
use Excel;
use MongoDB\BSON\ObjectID;
Use DateTimeZone;
//Models
//Helpers and Class

use Illuminate\Http\Request;


use local\Base\Manager\Controllers\ManagerController;

use local\Base\Manager\Facades\Utilities;

class DataTableController extends ManagerController {
	
	protected $datesFields = ['created_at', 'updated_at', 'fecha','fechaInicio', 'fechaFinal' ];


	protected $fieldsAgent = [
       
        'languages' => 'Lenguaje',
        'platform' => 'Sistema Operativo',
        'platformVersion' => 'Sistema Versión',
        'browser' => 'Navegador',
        'browserVersion' => 'Navegador Versión',
        'isDesktop' => 'Escritorio',
        'isRobot' => 'Robot',
        'device' => 'Dispositivo',
        'ip' => 'Ip',
        'pais' => 'Pais',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    
    

    protected $skip = null;
    protected $limit = null;
    protected $order = null;
    protected $collectionName = null;
    protected $fieldsDetailsExcel = null;
    protected $fieldsDetails = null;
    protected $searchable = [];
    protected $draw = null;
    protected $fields = null;

    public function getSkip() {

        return $this->skip;
    }

    public function setSkip($skip) {

        $this->skip = $skip;
    }

    public function getLimit() {

        return $this->limit;
    }

    public function setLimit($limit) {

        $this->limit = $limit;
    }

    public function getOrder() {

        return $this->order;
    }

    public function setOrder($order) {

        $this->order = $order;
    }

    protected function generateOrder($orders = [], $columns = []) {

        $order = [];

        foreach ($orders as $o) {

            if (isset($columns[$o['column']]) && !empty($columns[$o['column']]['data'])) {

                $column = $columns[$o['column']]['data'];

                $dir = ( $o['dir'] == 'asc' ) ? 1 : -1;

                $order[$column] = $dir;
            }
        }

        $this->setOrder($order);
    }

    protected function paginate($page = false, $perPage = false) {

        if ($page && $perPage) {

            $skip = ($page - 1) * $perPage;

            $limit = $perPage;

            $this->setSkip($skip);

            $this->setLimit($limit);
        }
    }

    private function executeConsult($query = [], $paginate = true, $order = false) {
        $options = [];


        if ($paginate) {

            $skip = $this->getSkip();

            $limit = $this->getLimit();

            if ($skip != null) {

                $options['skip'] = (int) $skip;
            }

            if ($limit != null) {

                $options['limit'] = (int) $limit;
            }
        }

        if ($order) {

            $orders = $this->getOrder();


            if (is_array($orders)) {
                $options['sort'] = $orders;
            }
        }



        $cursor = DB::collection($this->collectionName)->raw(function($collection) use ($query, $options) {

            return $collection->find($query, $options);
        });




        return $cursor;
    }

    private function executeConsultCount($query = [], $paginate = true, $order = false) {
        $options = [];


        if ($paginate) {

            $skip = $this->getSkip();

            $limit = $this->getLimit();

            if ($skip != null) {

                $options['skip'] = (int) $skip;
            }

            if ($limit != null) {

                $options['limit'] = (int) $limit;
            }
        }

        if ($order) {

            $orders = $this->getOrder();


            if (is_array($orders)) {
                $options['sort'] = $orders;
            }
        }
		

        $cursor = DB::collection($this->collectionName)->raw(function($collection) use ($query, $options) {

            return $collection->count($query, $options);
        });




        return $cursor;
    }

    protected function formatRecord($field, $item) {


        $this->formatTypeBson($field, $item);


        $record = '';

        if (($field == '_id' || $field == 'id') && isset($item[$field])) {

            $record = (string) $item['_id'];
        } else if (in_array($field, $this->datesFields) && isset($item[$field])) {

            $fecha = $item[$field];

            $fecha->setTimezone(new DateTimeZone('America/Mexico_City'));

            $record = $fecha->format("Y-m-d H:i");
            
        } else if (isset($item[$field])) {

            $record = $item[$field];
        }

        return $record;
    }

    protected function formatTypeBson($field, &$item) {


        if (isset($item[$field]) && gettype($item[$field]) == "object" && (get_class($item[$field]) == "MongoDB\Model\BSONArray" || get_class($item[$field]) == "MongoDB\Model\BSONDocument")) {

            $bsonSerialize = $item[$field]->bsonSerialize();
            

            $item[$field] = array_values((array)$bsonSerialize);
        } else if (isset($item[$field]) && gettype($item[$field]) == "object" && get_class($item[$field]) == "MongoDB\BSON\UTCDateTime") {

            $item[$field] = $item[$field]->toDateTime();
        }
    }

    protected function getRecordById($id) {

        $query = [

            '_id' => new ObjectID($id)
        ];

        $item = DB::collection($this->collectionName)->raw(function($collection) use ($query) {

            return $collection->findOne($query);
        });


        $data = [];


        foreach ($this->fieldsDetails as $k => $field) {

            if ($field) {
                $data[$field] = $this->formatRecordDetails($k, $item);
            }
        }



        return $data;
    }

    protected function formatRecordDetails($field, $item) {

        $record = $this->formatRecord($field, $item);

        return $record;
    }

    protected function formatRecordExcel($field, $item) {



        $record = $this->formatRecord($field, $item);

        return $record;
    }

    protected function generatedExcel($query) {

        $cursor = $this->executeConsult($query, FALSE, FALSE);



        $data = [];

        foreach ($cursor as $item) {




            $record = [];


            foreach ($this->fieldsDetailsExcel as $k => $field) {

                if ($field) {

                    $record[$field] = $this->formatRecordExcel($k, $item);
                }
            }


            $data[] = $record;


        }

        //dd($data);

        $headersExcel = array_values($this->fieldsDetailsExcel);


        $fileName = 'reporte_' . $this->collectionName . '_' . date('Y_m_d_H_i_s');

        Excel::create($fileName, function($excel) use($data, $headersExcel) {

            $excel->sheet($this->collectionName, function($sheet) use($data, $headersExcel) {
                // Manipulate first row
                $sheet->row(1, $headersExcel);

//->fromArray($source, $nullValue, $startCell, $strictNullComparison, $headingGeneration)
                $sheet->fromArray($data, null, 'A2', TRUE, FALSE);
            });
        })->store('xls', storage_path('excel/exports'))->export('xls');
    }

    protected function generatedDataTableResponse($query) {

        $cursor = $this->executeConsult($query, true, true);

        $data = [];


        foreach ($cursor as $item) {

            $record = [];

            foreach ($this->fields as $fieldArray) {

                $field = trim($fieldArray['data']);

                if ($field) {

                    $record[$field] = $this->formatRecord($field, $item);
                }
            }


            $data[] = $record;
        }

        $recordsFiltered = $this->executeConsultCount($query, false, false);

        $recordsTotal = count($data);

        return [
            "draw" => $this->draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            "data" => $data
        ];
    }

    protected function prepareRequest($inputs) {



        $this->draw = (isset($inputs['draw'])) ? $inputs['draw'] : 1;


        if (!isset($inputs['query']) && isset($inputs['search']['value'])) {

            $inputs['query'] = $inputs['search']['value'];

            unset($inputs['search']);
        }

        $page = (isset($inputs['start'])) ? ( ($inputs['start'] / $inputs['length']) + 1 ) : false;

        $perPage = (isset($inputs['length'])) ? $inputs['length'] : false;

        if ((isset($inputs['order'])) && (isset($inputs['columns']))) {

            $this->generateOrder($inputs['order'], $inputs['columns']);
        }
        $this->fields = (isset($inputs['fields'])) ? $inputs['fields'] : [];



        $this->paginate($page, $perPage);

        return $inputs;
    }
    
    
      public function dataTable(Request $request, $id = false, $excel = false) {

		  

        $query = [];


        $inputs = $request->all();


        if ($id != FALSE) {


            $isValid = Utilities::idMongoValid($id);

            if ($isValid) {

                return response()->json($this->getRecordById($id));
            } else {
                return response()->json([]);
            }
        } else {


            $inputs = $this->prepareRequest($inputs);

            $query = Utilities::generateQuery($inputs, $this->searchable);
			
			
//			dd($query);


            if ($excel) {
                
                $this->generatedExcel($query);
            } else {
                
                $return = $this->generatedDataTableResponse($query);

                return response()->json($return);
            }
        }
    }


}
