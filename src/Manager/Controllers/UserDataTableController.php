<?php

namespace local\Base\Manager\Controllers;

//Providers
//Models
use local\Base\Manager\Models\User;
//Helpers and Class
use local\Base\Manager\Controllers\DataTableController;

class UserDataTableController extends DataTableController {

    protected $collectionName = 'users';
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'nombre' => 'contains',
            'apellido' => 'contains',
            'email' => 'contains',
            'idPayer' => 'contains'
        ],
        'genero' => 'equal',
        'recibo' => 'equal',
    ];
    protected $fieldsDetails = [
        '_id' => 'ID',
        'idPayer' => 'id Paypal',
        'nombre' => 'Nombre',
        'apellido' => 'apellido',
        'email' => 'Email',
        // 'telefono' => 'Teléfono',
        // 'ciudad' => 'Ciudad',
        // 'edad' => 'Edad',
        // 'genero' => 'Genero',
        'recibo' => 'Recibo',
        'razonSocial' => 'Razón Social',
        'rfc' => 'RFC',
        'domicilioFiscal' => 'Fiscal',
        'languages' => 'Lenguaje',
        'platform' => 'Sistema Operativo',
        'platformVersion' => 'Sistema Versión',
        'browser' => 'Navegador',
        'browserVersion' => 'Navegador Versión',
        'isDesktop' => 'Escritorio',
        'isRobot' => 'Robot',
        'device' => 'Dispositivo',
        'ip' => 'Ip',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'ID',
        'idPayer' => 'id Paypal',
        'nombre' => 'Nombre',
        'apellido' => 'apellido',
        'email' => 'Email',
        // 'telefono' => 'Teléfono',
        // 'ciudad' => 'Ciudad',
        // 'edad' => 'Edad',
        // 'genero' => 'Genero',
        'recibo' => 'Recibo',
        'razonSocial' => 'Razón Social',
        'rfc' => 'RFC',
        'domicilioFiscal' => 'Fiscal',
        //'languages' => 'Lenguaje',
        'platform' => 'Sistema Operativo',
        'platformVersion' => 'Sistema Versión',
        'browser' => 'Navegador',
        'browserVersion' => 'Navegador Versión',
        'isDesktop' => 'Escritorio',
        'isRobot' => 'Robot',
        'device' => 'Dispositivo',
        'ip' => 'Ip',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);




        if ($field == 'url_details') {

            $record = route('manager.user.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {


            $userRelationships = config('manager.paypal.userRelationships');

            $user = $userRelationships::find($item['_id']);
            $numPays = $user->paypal()->count();

            $record = "<a href=" . route('manager.paypal.index', ['s' => (string) $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Donativos' > {$numPays} <i class='fa fa-money'></i> </a>";

            if ($item['status'] === TRUE) {

                $record .= "<a href=" . route('manager.user.destroy', ['id' => $item['_id']]) . "  class='text-center center-block desactivar'  data-toggle='tooltip' title='Click para Desactivar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {

                $record .= "<a href=" . route('manager.user.destroy', ['id' => $item['_id']]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Click para Habilitar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

}
