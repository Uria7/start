<?php

namespace local\Base\Manager\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ManagerController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    protected $data = [];

    public function __construct() {


        setlocale(LC_ALL, 'es_MX');
        
        $this->middleware(function ($request, $next) {

            $this->data['user'] = \Auth::user();

            return $next($request);
        });

        $s = request()->input('s');
        if (!empty($s)) {
            \View::share('s', $s);
        }
    }

}
