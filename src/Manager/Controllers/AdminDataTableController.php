<?php

namespace local\Base\Manager\Controllers;
//Providers
//Models
//Helpers and Class


use local\Base\Manager\Controllers\DataTableController;


class AdminDataTableController extends DataTableController {

    protected $collectionName = 'admin';
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'nombre' => 'contains',
            'email' => 'contains',
        ],
        'rol' => 'equal',
        'status' => 'equal',
    ];
    protected $fieldsDetails = [
        '_id' => 'ID',
        'nombre' => 'Nombre',
        'email' => 'Email',
        'descripcion' => 'Descripción',
        'rol' => 'Rol',
        'genero' => 'Género',
        'status' => 'Estatus',
        
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'ID',
        'nombre' => 'Nombre',
        'email' => 'Email',
        'descripcion' => 'Descripción',
        'rol' => 'Rol',
        'genero' => 'Género',
        'status' => 'Estatus',
        
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'status') {


            $record = ($item[$field]) ? 'Activo' : 'Inactivo';
        } else if ($field == 'url_details') {

            $record = route('manager.admin.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {


            $record = "<a href=" . route('manager.admin.edit', ['id' => $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";

            if ($item['status'] === TRUE) {

                $record .= "<a href=" . route('manager.admin.destroy', ['id' => $item['_id']]) . "  class='text-center center-block desactivar'  data-toggle='tooltip' title='Click para Desactivar'  > "
                        . "<i class='fa fa-trash-o'></i> "
                        . "</a>";
            } else {

                $record .= "<a href=" . route('manager.admin.destroy', ['id' => $item['_id']]) . "  class='text-center center-block recuperar'  data-toggle='tooltip' title='Click para Habilitar'  > "
                        . "<span class='fa-stack fa'>"
                        . "<i class='fa fa-trash-o fa-stack-1x '></i>"
                        . "<i class='fa fa-ban fa-stack-2x text-danger'></i>"
                        . "</span>"
                        . "</a>";
            }
        }

        return $record;
    }

}
