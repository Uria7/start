<?php

namespace local\Base\Manager\Controllers;

//Providers

//Models
//Helpers and Class


use local\Base\Manager\Controllers\ManagerController;
use Auth;
use ImageInter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Validator;

class ImageController extends ManagerController {

    public function load(Request $request) {


        $input = $request->all();
        
        
        
        $disk = $request->input('disk', 'public');
        
        
        $rules = array(
            'image' => array('required'),
            'path' => array('required'),
            'format' => array('required'),
            'quality' => array('required'),
            'w' => array('required'),
            'h' => array('required')
        );

        $validator = Validator::make($input, $rules);


        $response = [];

        if ($validator->fails()) {

            $response['success'] = false;
            $response['messages'] = $validator->messages()->all();
        } else if (!$request->file('image')->isValid()) {

            $response['success'] = false;
            $response['messages'] = 'Imagen no valida';
        } else {

			
			

            $folder = $input['path'];

            $format = $input['format'];

            $file = $request->file('image');
			
            
            $img = ImageInter::make($file);


            if (!isset($input['resize'])) {
                $input['resize'] = null;
            }


            switch ($input['resize']) {
                case 'fit':
                    $img = $img->fit((int) $input['w'], (int) $input['h']);

                    break;

                case 'widen':
                    $img = $img->widen((int) $input['w']);

                    break;
                case 'heighten':
                    $img = $img->heighten((int) $input['h']);
                    break;
                case 'resize':
                    $img = $img->resize((int) $input['w'], (int) $input['h']);
                    break;

                default:
                    $img = $img->resize((int) $input['w'], (int) $input['h']);
                    break;
            }

            if (isset($input['fill'])) {

                $imgBg = ImageInter::canvas($img->width(), $img->height(), $input['fill']);


                $imgBg->insert($img);

                $img = $imgBg;
            }


            $fileData = (string) $img->encode($format, (int) $input['quality']);


            $path = $this->hashName($file, $folder, $format);

            $isSave = Storage::disk($disk)->put($path, $fileData);
			
			
			$httpPath = 'storage/' . $path;
			
			
			if ((isset($input['baseUrl']) && $input['baseUrl']==='full')) {
                $httpPath = asset($httpPath);
			}


            if ($isSave) {

                $response['success'] = true;
                $response['data'] =  $httpPath ;
            } else {
                $response['success'] = false;
                $response['messages'] = "No se encontro imagen";
            }
        }


        return response()->json($response);
    }
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data = array();

		$data['user'] = Auth::user();

		 return view('baseViews::pageEditFormImagen', $data);
	}



    public function hashName(UploadedFile $file, $path = null, $extension = null) {
        if ($path) {
            $path = rtrim($path, '/') . '/';
        }

        $ext = (is_null($extension)) ? '' : '.' . $extension;


        return $path . md5_file($file->getRealPath()) . $ext;
    }

}
