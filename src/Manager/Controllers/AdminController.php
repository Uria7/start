<?php

namespace local\Base\Manager\Controllers;

//Facades
use Validator;
use Auth;
//Models
use local\Base\Manager\Models\Admin;
//Class
use local\Base\Manager\Controllers\ManagerController;
use Illuminate\Http\Request;
use local\Base\Manager\Traits\DynamicInputs;
use local\Base\Manager\Facades\Utilities;

class AdminController extends ManagerController {

    use DynamicInputs;

    protected $configPages = '';
    protected $returnRoute = 'manager.admin.index';

    public function __construct() {
        parent::__construct();
        $this->configPages = config('manager.admins');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        return view('baseViews::adminPanel', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {


        $this->data['dataPage'] = [];



        $type = request('type', 'index');
        $configPage = $this->configPages[$type];

        $processData = $this->processPageUI($configPage, $this->data);


        return view('baseViews::adminAddForm', $processData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $input = $request->all();
        $rules = array(
            'nombre' => array('required'),
            'email' => array('required', 'email', 'unique:admin,email'),
            'password' => array('required', 'min:6'),
            'rol' => array('required')
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {


            $admin = new Admin;
            $admin->fill($input);



            $type = request('type', 'index');
            $sections = $this->configPages[$type]['sections'];


            $admin = $this->dynamicFillSave($admin, $sections, $input);


            $admin->save();

            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $isValid = Utilities::idMongoValid($id);
        $record = Admin::findOrFail($id);


        if (!$isValid || !isset($record->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }



        $this->data['record'] = $record;



        $this->data['dataPage'] = $record->toArray();


        $type = request('type', 'index');
        $configPage = $this->configPages[$type];

        $processData = $this->processPageUI($configPage, $this->data);

        return view('baseViews::adminEditForm', $processData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $rules = array(
            'nombre' => array('required'),
            'password' => array('min:6'),
            'rol' => array('required'),
            'genero' => array('required'),
            'status' => array('required')
        );


        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
            ]);
        } else {


            $isValid = Utilities::idMongoValid($id);
            $admin = Admin::find($id);

            if (!$isValid || !isset($admin->id)) {
                return back()->with([
                            'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
                ]);
            }

            if (empty($input['password'])) {

                unset($input['password']);
            }



            $admin->fill($input);


            $type = request('type', 'index');
            $sections = $this->configPages[$type]['sections'];


            $admin = $this->dynamicFillSave($admin, $sections, $input);



            $admin->save();


            return redirect()->route($this->returnRoute)->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $authUser = Auth::user();

        $isValid = Utilities::idMongoValid($id);
        $admin = Admin::findOrFail($id);

        if (!$isValid || !isset($admin->id) || $authUser->id == $admin->id) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }

        if ($admin->status) {

            $admin->status = FALSE;
        } else {
            $admin->status = TRUE;
        }



        $admin->save();


        return redirect()->route($this->returnRoute)->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
    }

}
