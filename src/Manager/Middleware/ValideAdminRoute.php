<?php

namespace local\Base\Manager\Middleware;

use Closure;
//Providers
use Auth;

class ValideAdminRoute {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        $user = auth()->user();
        if ($user->rol !='admin') {
            return back()->with([
                        'error' => trans('baseLang::mensajes.session.privilegios'),
                    ]);
        }



        return $next($request);
    }

}
