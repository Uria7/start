<?php
namespace local\Base\Manager\Middleware;



use Closure;
use Illuminate\Contracts\Auth\Factory as AuthFactory;

class AuthenticateManager
{
    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(AuthFactory $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        
        
        
         if (\Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                 $redirect = config('manager.base.manager.redirectLogout','manager.index');
                
                    
                    return redirect()->route($redirect)->with([
                        'error' => trans('baseLang::mensajes.usuario.noEncontrado'),
            ]);
                
                
                
            }
        }

        return $next($request);
    }
}
