<?php

namespace local\Base\Manager\Middleware;

use Closure;
//use Illuminate\Support\Facades\Auth;

class setGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'web')
    {
        
       config(['auth.defaults.guard'=> $guard]);
        

        return $next($request);
    }
}
