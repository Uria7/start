<?php

namespace local\Base\Manager\Middleware;

use Closure;
//Providers
use Auth;

class ValideEditRoute {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        
        $user = auth()->user();
        if ($user->rol !='editor' && $user->rol !='admin') {
//            return redirect()->route("registros.panel")->with([
//                        'error' => trans('mensajes.session.privilegios'),
//                    ]);
            
             return back()->with([
                        'error' => trans('baseLang::mensajes.session.privilegios'),
                    ]);
        }



        return $next($request);
    }

}
