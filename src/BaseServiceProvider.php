<?php

namespace local\start;

use Route;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Routing\Router;

class BaseServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
//	protected $defer = true;


    private $configBase = __DIR__ . '/../config/manager/base.php';
    private $configPages = __DIR__ . '/../config/manager/pages.php';
    private $configAdmins = __DIR__ . '/../config/manager/admins.php';
    private $folderLang = __DIR__ . '/Manager/resources/lang';
    private $folderViews = __DIR__ . '/Manager/resources/views';
    private $routeMiddleware = [
        'authManager' => \local\start\Manager\Middleware\AuthenticateManager::class,
        'adminRoute' => \local\start\Manager\Middleware\ValideAdminRoute::class,
        'editRoute' => \local\start\Manager\Middleware\ValideEditRoute::class,
        'setGuard' => \local\start\Manager\Middleware\setGuard::class,
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router) {


//        Publishes Configuración Base
//        php artisan vendor:publish --provider="local\Base\BaseServiceProvider" --tag=config

        $this->publishes([
            $this->configBase => config_path('manager/base.php'),
            $this->configPages => config_path('manager/pages.php'),
            $this->configAdmins => config_path('manager/admins.php')
                ], 'config');


//        Registra Views
        $this->loadViewsFrom($this->folderViews, 'baseViews');

//        php artisan vendor:publish --provider="local\Base\BaseServiceProvider" --tag=lang 
        $this->loadTranslationsFrom($this->folderLang, 'baseLang');
        $this->publishes([
            $this->folderLang => resource_path('lang/vendor/baseLang'
            )], 'lang');

    

        foreach ($this->routeMiddleware as $key => $middleware) {
            $router->middleware($key, $middleware);
        }




//ROUTE LOGIN 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'local\start\Manager\Controllers';

        $originalMiddleware = $config['middleware'];

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/login.php');
        });



        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'adminRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/admin.php');
        });


        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/pages.php');
        });


        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/user.php');
        });
        
        
        
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/image.php');
        });
        
        
        
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/fileUpload.php');
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {


        $this->mergeConfigFrom($this->configBase, 'manager.base');
        $this->mergeConfigFrom($this->configPages, 'manager.pages');
        $this->mergeConfigFrom($this->configAdmins, 'manager.admins');







        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        App::bind('Utilities', function() {
            return new \local\start\Manager\Controllers\Utilities;
        });
        $loader->alias('Utilities', \local\start\Manager\Facades\Utilities::class);
    }

}
