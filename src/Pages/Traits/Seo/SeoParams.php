<?php

namespace local\Base\Pages\Traits\Seo;
use Arcanedev\SeoHelper\Traits\Seoable;

trait SeoParams {

	use Seoable;

	public function setSeoParams($pagina) {


		if (isset($pagina->titleSeo)) {
			$this->seo()
					->setTitle($pagina->titleSeo);
		}

		if (isset($pagina->descriptionSeo)) {
			$this->seo()
					->setDescription($pagina->descriptionSeo);
		}

		if (isset($pagina->keywordsSeo)) {
			$this->seo()
					->setKeywords($pagina->keywordsSeo);
		}

		

		if (isset($pagina->imageSeo)) {

			$this->seoGraph()
					->setImage(asset($pagina->imageSeo));
		}
	}

}
