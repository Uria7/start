<?php

namespace local\Base\Pages\Controllers;

use App\Http\Controllers\Controller;

class PagesController extends Controller {


    protected $data = [];
    protected $locale = 'es';

    public function __construct() {
        
        parent::__construct();

        
        $this->locale = \App::getLocale();
        setlocale(LC_ALL, 'es_MX');

        $this->middleware(function ($request, $next) {

            $this->data['user'] = \Auth::user();

            return $next($request);
        });
    }

}
